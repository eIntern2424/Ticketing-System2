﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketingSystem4.Models.Entities
{
    public class Technician
    {
        [DatabaseGenerated(databaseGeneratedOption:DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name ="First Name:")]
        public string FName { get; set; }
        [Required]
        [Display(Name ="Last Name:")]
        public string LName { get; set; }
        [Display(Name ="Department:")]
        public int DepartmentID { get; set; }
        [Display(Name ="Email:")]
        public string Username { get; set; }

        public virtual ICollection<Ticket>Tickets { get; set; }
        
    }
}
