﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TicketingSystem4.Models.Entities
{
    public class Customer
    {
        [DatabaseGenerated(databaseGeneratedOption:DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name ="First Name")]
        public string FName { get; set; }

        [Required]
        [Display(Name ="LastName")]
        public string LName { get; set; }

        [Required]
        [Display(Name ="Email")]
        public string Username { get; set; }

        public virtual ICollection<Ticket>Tickets { get; set; }


    }
}
