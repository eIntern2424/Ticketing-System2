﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketingSystem4.Models.Entities
{
    public class Ticket
    {
        [DatabaseGenerated(databaseGeneratedOption:DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [Display(Name ="First Name")]
        public string CreFName { get; set; }

        [Required]
        [Display(Name ="Last Name")]
        public string CreLName { get; set; }
        [Display(Name ="Assigned Technician")]
        public int TechnicianID { get; set; }

        //public int CustomerID { get; set; }
        [Display(Name ="Progress")]
        public  int ProgressID { get; set; }

        [Display(Name ="Suggested Solution")]
        public int SolutionID { get; set; }

        [Display(Name ="Handling Department")]
        public int DepartmentID { get; set; }

        [Display(Name ="Problem Description")]
        public string Description { get; set; }

        [Display(Name ="Email Address:")]
        public string Username { get; set; }
        public string TUsername { get; set; }
    }
}
