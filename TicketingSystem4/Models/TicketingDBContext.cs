﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketingSystem4.Models.Entities;
using System.Web;

namespace TicketingSystem4.Models
{
    public class TicketingDBContext : DbContext
    {
        public TicketingDBContext() : base("TicketingSystem")
        {

        }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Progress> Progress { get; set; }
        public DbSet<Technician> Technicians { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Solution> Solutions { get; set; }

    }

}
