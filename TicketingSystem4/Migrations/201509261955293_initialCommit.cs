namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialCommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FName = c.String(nullable: false),
                        LName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CreFName = c.String(nullable: false),
                        CreLName = c.String(nullable: false),
                        Customer_ID = c.Int(),
                        Department_ID = c.Int(),
                        Progress_ID = c.Int(),
                        Solution_ID = c.Int(),
                        Technician_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.Customer_ID)
                .ForeignKey("dbo.Departments", t => t.Department_ID)
                .ForeignKey("dbo.Progresses", t => t.Progress_ID)
                .ForeignKey("dbo.Solutions", t => t.Solution_ID)
                .ForeignKey("dbo.Technicians", t => t.Technician_ID)
                .Index(t => t.Customer_ID)
                .Index(t => t.Department_ID)
                .Index(t => t.Progress_ID)
                .Index(t => t.Solution_ID)
                .Index(t => t.Technician_ID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Progresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Solutions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SolutionType = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Technicians",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FName = c.String(nullable: false),
                        LName = c.String(nullable: false),
                        Department_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Departments", t => t.Department_ID)
                .Index(t => t.Department_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Technician_ID", "dbo.Technicians");
            DropForeignKey("dbo.Technicians", "Department_ID", "dbo.Departments");
            DropForeignKey("dbo.Tickets", "Solution_ID", "dbo.Solutions");
            DropForeignKey("dbo.Tickets", "Progress_ID", "dbo.Progresses");
            DropForeignKey("dbo.Tickets", "Department_ID", "dbo.Departments");
            DropForeignKey("dbo.Tickets", "Customer_ID", "dbo.Customers");
            DropIndex("dbo.Technicians", new[] { "Department_ID" });
            DropIndex("dbo.Tickets", new[] { "Technician_ID" });
            DropIndex("dbo.Tickets", new[] { "Solution_ID" });
            DropIndex("dbo.Tickets", new[] { "Progress_ID" });
            DropIndex("dbo.Tickets", new[] { "Department_ID" });
            DropIndex("dbo.Tickets", new[] { "Customer_ID" });
            DropTable("dbo.Technicians");
            DropTable("dbo.Solutions");
            DropTable("dbo.Progresses");
            DropTable("dbo.Departments");
            DropTable("dbo.Tickets");
            DropTable("dbo.Customers");
        }
    }
}
