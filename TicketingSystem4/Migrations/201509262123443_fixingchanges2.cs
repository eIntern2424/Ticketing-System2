namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixingchanges2 : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Tickets", "ProgressID");
            CreateIndex("dbo.Tickets", "SolutionID");
            AddForeignKey("dbo.Tickets", "ProgressID", "dbo.Progresses", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Tickets", "SolutionID", "dbo.Solutions", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "SolutionID", "dbo.Solutions");
            DropForeignKey("dbo.Tickets", "ProgressID", "dbo.Progresses");
            DropIndex("dbo.Tickets", new[] { "SolutionID" });
            DropIndex("dbo.Tickets", new[] { "ProgressID" });
        }
    }
}
