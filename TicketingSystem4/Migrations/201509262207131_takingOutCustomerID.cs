namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class takingOutCustomerID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tickets", "CustomerID", "dbo.Customers");
            DropIndex("dbo.Tickets", new[] { "CustomerID" });
            RenameColumn(table: "dbo.Tickets", name: "CustomerID", newName: "Customer_ID");
            AlterColumn("dbo.Tickets", "Customer_ID", c => c.Int());
            CreateIndex("dbo.Tickets", "Customer_ID");
            AddForeignKey("dbo.Tickets", "Customer_ID", "dbo.Customers", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Customer_ID", "dbo.Customers");
            DropIndex("dbo.Tickets", new[] { "Customer_ID" });
            AlterColumn("dbo.Tickets", "Customer_ID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Tickets", name: "Customer_ID", newName: "CustomerID");
            CreateIndex("dbo.Tickets", "CustomerID");
            AddForeignKey("dbo.Tickets", "CustomerID", "dbo.Customers", "ID", cascadeDelete: true);
        }
    }
}
