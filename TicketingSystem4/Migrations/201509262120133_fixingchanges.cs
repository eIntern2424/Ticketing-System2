namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixingchanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tickets", "Department_ID", "dbo.Departments");
            DropForeignKey("dbo.Tickets", "Progress_ID", "dbo.Progresses");
            DropForeignKey("dbo.Tickets", "Solution_ID", "dbo.Solutions");
            DropForeignKey("dbo.Tickets", "Customer_ID", "dbo.Customers");
            DropForeignKey("dbo.Tickets", "Technician_ID", "dbo.Technicians");
            DropForeignKey("dbo.Technicians", "Department_ID", "dbo.Departments");
            DropIndex("dbo.Tickets", new[] { "Customer_ID" });
            DropIndex("dbo.Tickets", new[] { "Department_ID" });
            DropIndex("dbo.Tickets", new[] { "Progress_ID" });
            DropIndex("dbo.Tickets", new[] { "Solution_ID" });
            DropIndex("dbo.Tickets", new[] { "Technician_ID" });
            DropIndex("dbo.Technicians", new[] { "Department_ID" });
            RenameColumn(table: "dbo.Tickets", name: "Customer_ID", newName: "CustomerID");
            RenameColumn(table: "dbo.Tickets", name: "Technician_ID", newName: "TechnicianID");
            RenameColumn(table: "dbo.Technicians", name: "Department_ID", newName: "DepartmentID");
            AddColumn("dbo.Customers", "Username", c => c.String(nullable: false));
            AddColumn("dbo.Tickets", "ProgressID", c => c.Int(nullable: false));
            AddColumn("dbo.Tickets", "SolutionID", c => c.Int(nullable: false));
            AddColumn("dbo.Tickets", "DepartmentID", c => c.Int(nullable: false));
            AlterColumn("dbo.Tickets", "CustomerID", c => c.Int(nullable: false));
            AlterColumn("dbo.Tickets", "TechnicianID", c => c.Int(nullable: false));
            AlterColumn("dbo.Technicians", "DepartmentID", c => c.Int(nullable: false));
            CreateIndex("dbo.Tickets", "TechnicianID");
            CreateIndex("dbo.Tickets", "CustomerID");
            CreateIndex("dbo.Technicians", "DepartmentID");
            AddForeignKey("dbo.Tickets", "CustomerID", "dbo.Customers", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Tickets", "TechnicianID", "dbo.Technicians", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Technicians", "DepartmentID", "dbo.Departments", "ID", cascadeDelete: true);
            DropColumn("dbo.Tickets", "Department_ID");
            DropColumn("dbo.Tickets", "Progress_ID");
            DropColumn("dbo.Tickets", "Solution_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "Solution_ID", c => c.Int());
            AddColumn("dbo.Tickets", "Progress_ID", c => c.Int());
            AddColumn("dbo.Tickets", "Department_ID", c => c.Int());
            DropForeignKey("dbo.Technicians", "DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.Tickets", "TechnicianID", "dbo.Technicians");
            DropForeignKey("dbo.Tickets", "CustomerID", "dbo.Customers");
            DropIndex("dbo.Technicians", new[] { "DepartmentID" });
            DropIndex("dbo.Tickets", new[] { "CustomerID" });
            DropIndex("dbo.Tickets", new[] { "TechnicianID" });
            AlterColumn("dbo.Technicians", "DepartmentID", c => c.Int());
            AlterColumn("dbo.Tickets", "TechnicianID", c => c.Int());
            AlterColumn("dbo.Tickets", "CustomerID", c => c.Int());
            DropColumn("dbo.Tickets", "DepartmentID");
            DropColumn("dbo.Tickets", "SolutionID");
            DropColumn("dbo.Tickets", "ProgressID");
            DropColumn("dbo.Customers", "Username");
            RenameColumn(table: "dbo.Technicians", name: "DepartmentID", newName: "Department_ID");
            RenameColumn(table: "dbo.Tickets", name: "TechnicianID", newName: "Technician_ID");
            RenameColumn(table: "dbo.Tickets", name: "CustomerID", newName: "Customer_ID");
            CreateIndex("dbo.Technicians", "Department_ID");
            CreateIndex("dbo.Tickets", "Technician_ID");
            CreateIndex("dbo.Tickets", "Solution_ID");
            CreateIndex("dbo.Tickets", "Progress_ID");
            CreateIndex("dbo.Tickets", "Department_ID");
            CreateIndex("dbo.Tickets", "Customer_ID");
            AddForeignKey("dbo.Technicians", "Department_ID", "dbo.Departments", "ID");
            AddForeignKey("dbo.Tickets", "Technician_ID", "dbo.Technicians", "ID");
            AddForeignKey("dbo.Tickets", "Customer_ID", "dbo.Customers", "ID");
            AddForeignKey("dbo.Tickets", "Solution_ID", "dbo.Solutions", "ID");
            AddForeignKey("dbo.Tickets", "Progress_ID", "dbo.Progresses", "ID");
            AddForeignKey("dbo.Tickets", "Department_ID", "dbo.Departments", "ID");
        }
    }
}
