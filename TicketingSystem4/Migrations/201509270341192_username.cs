namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class username : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Technicians", "Username", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Technicians", "Username");
        }
    }
}
