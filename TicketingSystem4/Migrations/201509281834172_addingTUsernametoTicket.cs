namespace TicketingSystem4.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingTUsernametoTicket : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tickets", "TUsername", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tickets", "TUsername");
        }
    }
}
