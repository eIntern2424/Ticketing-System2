﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TicketingSystem4.Models;
using TicketingSystem4.Models.Entities;
using TicketingSystem4.Infrastructure;

namespace TicketingSystem4.Controllers
{
    public class TechniciansController : Controller
    {
        private TicketingDBContext db = new TicketingDBContext();
        private Helper H = new Helper();
        // GET: Technicians
       [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View(db.Technicians.ToList());
        }

        // GET: Technicians/Details/5
        [Authorize(Roles ="Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Technician technician = db.Technicians.Find(id);

            // This is how we link the department name to match the department ID of the technician
            // We are going into the department table and taking a representation of a department and finding where the 
            //department ID of the technician matches up with the id of the representation  
            //and display the name from the department table that matches with the ID from the selected item from the dropdown.
            string dpt = db.Departments.Where(d => d.ID == technician.DepartmentID).First().Description;
            if (technician == null)
            {
                return HttpNotFound();
            }
            ViewBag.department = dpt;
            return View(technician);
        }

        // GET: Technicians/Create
       [Authorize(Roles ="Admin")]
        public ActionResult Create()
        {
            ViewBag.departmentDropDown = H.DepartmentDropDown();
            return View();
        }

        // POST: Technicians/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
       [Authorize(Roles ="Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FName,LName,DepartmentID,Username")] Technician technician)
        {

            if (ModelState.IsValid)
            {
                if (db.Technicians.Where(c => c.FName == technician.FName && c.LName == technician.LName).Count() <= 0)// Checking if technician exists
                {
                    ApplicationUser newtechnician = new ApplicationUser // creates an instance of a technician object
                    {

                        Email = technician.Username,
                        UserName = technician.Username,

                    };

                    ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); // The app manager is what creates user to put into the database
                    UserManager.Create(newtechnician, "Password1!"); //The create method is what creates the technician account with the default password
                    var currentUser = UserManager.FindByName(technician.Username); // not sure...may be what displays "Hello 'User'" in the home/index view
                    UserManager.AddToRole(newtechnician.Id, "Technician"); // This adds the user to the technician role
                    db.Technicians.Add(technician);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            //ViewBag.departmentDropDown = H.DepartmentDropDown();
            return View(technician);
        }

        // GET: Technicians/Edit/5
       [Authorize(Roles ="Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Technician technician = db.Technicians.Find(id);
            if (technician == null)
            {
                return HttpNotFound();
            }
            ViewBag.departmentDropDown = H.DepartmentDropDown();
            return View(technician);
        }

        // POST: Technicians/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles ="Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FName,LName,DepartmentID")] Technician technician)
        {
            if (ModelState.IsValid)
            {
                db.Entry(technician).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(technician);
        }

        // GET: Technicians/Delete/5
        [Authorize(Roles ="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Technician technician = db.Technicians.Find(id);
            if (technician == null)
            {
                return HttpNotFound();
            }
            return View(technician);
        }

        // POST: Technicians/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Technician technician = db.Technicians.Find(id);
            db.Technicians.Remove(technician);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
