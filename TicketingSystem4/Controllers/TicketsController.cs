﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TicketingSystem4.Models;
using TicketingSystem4.Models.Entities;
using TicketingSystem4.Infrastructure;
namespace TicketingSystem4.Controllers
{
    public class TicketsController : Controller
    {
        private TicketingDBContext db = new TicketingDBContext();
        private Helper H = new Helper();

        // GET: Tickets
        [Authorize(Roles ="Customer,Admin,Technician")]
        public ActionResult Index()
        {

            return View(db.Tickets.ToList());
        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            string solution = db.Solutions.Where(s => s.ID == ticket.SolutionID).First().SolutionType;
            string fname = db.Technicians.Where(t => t.ID == ticket.TechnicianID).First().FName;
            string lname = db.Technicians.Where(t => t.ID == ticket.TechnicianID).First().LName;
            string name = fname + " " + lname;
            string department = db.Departments.Where(d => d.ID == ticket.DepartmentID).First().Description;
            string progress = db.Progress.Where(p => p.ID == ticket.ProgressID).First().Description;
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ViewBag.solution = solution;
            ViewBag.name = name;
            ViewBag.department = department;
            ViewBag.progress = progress;
            return View(ticket);
        }

        // GET: Tickets/Create
        [Authorize(Roles = "Customer,Admin,Technician")]
        public ActionResult Create()
        {
            //ViewBag.solutionDropDown = H.SolutionDropDown();
            //ViewBag.progressDropDown = H.ProgressDropDown();
            ViewBag.departmentDropDown = H.DepartmentDropDown();
            //ViewBag.technicianDropDown = H.TechnicianDropDown();
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CreFName,CreLName,CustomerID,Description,Username,DepartmentID")] Ticket ticket)
        {
            
            if (ModelState.IsValid)
            {
                var queryTechAssign = from tech in db.Technicians
                                      where tech.DepartmentID == ticket.DepartmentID
                                      orderby tech.Tickets.Count() ascending
                                      select tech;
                ticket.TechnicianID = queryTechAssign.FirstOrDefault().ID;

                ticket.ProgressID = 1;
               // ticket.DepartmentID = 3;
                ticket.SolutionID = 1;
                //ticket.TechnicianID = 5;
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(ticket);
        }

        // GET: Tickets/Edit/5
        [Authorize(Roles ="Admin,Technician")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ViewBag.progressDropDown = H.ProgressDropDown();
            ViewBag.solutionDropDown = H.SolutionDropDown();
            ViewBag.departmentDropDown = H.DepartmentDropDown();
            ViewBag.technicianDropDown = H.TechnicianDropDown();
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CreFName,CreLName,CustomerID,ProgressID,DepartmentID,Description,TechnicianID,SolutionID")] Ticket ticket)
        {
            ViewBag.departmentDropDown = H.DepartmentDropDown();
            if (ModelState.IsValid)
            {
                if (ticket.TechnicianID == db.Technicians.Where(t=>t.FName=="Unassigned").FirstOrDefault().ID)
                {
                    var queryTechAssign = from tech in db.Technicians
                                          where tech.DepartmentID == ticket.DepartmentID
                                          orderby tech.Tickets.Count() ascending
                                          select tech;
                    ticket.TechnicianID = queryTechAssign.FirstOrDefault().ID;
                }
                ticket.TUsername = db.Technicians.Find(ticket.TechnicianID).Username;
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult TechnicianIndex()
        {
            return View(db.Tickets.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
