﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketingSystem4.Models;
using TicketingSystem4.Models.Entities;

namespace TicketingSystem4.Infrastructure
{
   public class Helper
    {
        private TicketingDBContext db = new TicketingDBContext();

        public List<object> SolutionDropDown()
        {
            List<object> solutionDropDown = new List<object>();
            var solutionList = db.Solutions.ToList();
            foreach (var solution in solutionList)
            {
                solutionDropDown.Add(new
                {
                    id = solution.ID,
                    name = solution.SolutionType
                });
            }

            return solutionDropDown;
        }

        public List<object> TechnicianDropDown()
        {
            List<object> technicianDropDown = new List<object>();
            var technicianList = db.Technicians.ToList();
            foreach(var technician in technicianList)
            {
                technicianDropDown.Add(new
                {
                    id = technician.ID,
                    name = technician.FName + " " + technician.LName
                });
            }
            return technicianDropDown;
        }

        public List<object> ProgressDropDown()
        {
            List<object> progressDropDown = new List<object>();
            var progressList = db.Progress.ToList();
            foreach(var progress in progressList)
            {
                progressDropDown.Add(new
                {
                    id = progress.ID,
                    name = progress.Description
                });
            }

            return progressDropDown;
        }

        public List<object> DepartmentDropDown()
        {
            List<object> departmentDropDown = new List<object>();

            var departmentList = db.Departments.ToList();

            foreach (var department in departmentList)
            {
                departmentDropDown.Add(new
                {
                    id = department.ID,
                    name = department.Description
                });
            }
            return departmentDropDown;
        }

    }
}
